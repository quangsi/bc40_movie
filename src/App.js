import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import Layout from "./Layout/Layout";
import BookingPage from "./Pages/BookingPage/BookingPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
function App() {
  return (
    <div className="min-h-screen ">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route
            path="/booking/:id"
            element={<Layout Component={BookingPage} />}
          />
          <Route path="*" element={<Layout Component={NotFoundPage} />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

// 404 page codepen
// page not found codepen
