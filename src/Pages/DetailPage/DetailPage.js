import { Progress } from "antd";
import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { movieServ } from "../../service/movieService";

export default function DetailPage() {
  let { id } = useParams();
  const [movie, setMovie] = useState({});
  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let result = await movieServ.getDetailMovie(id);
        setMovie(result.data.content);
        console.log("result", result);
      } catch (error) {
        console.log("error", error);
      }
    };
    fetchDetail();
  }, []);

  //   aysnc await ~ the catch
  return (
    <div className="container">
      <div className="flex space-x-10">
        <img src={movie.hinhAnh} className="w-1/3" alt="" />
        <div className="space-y-5">
          <h2 className="font-medium">{movie.tenPhim}</h2>
          <h2>{movie.moTa}</h2>
          <Progress percent={movie.danhGia * 10} />
        </div>
      </div>
      <NavLink
        className="rounded px-5 py-2 bg-red-600 text-white font-medium"
        to={`/booking/${id}`}
      >
        Mua vé
      </NavLink>
      <button className="rounded px-5 py-2 bg-red-600 text-white font-medium">
        ok{" "}
      </button>
    </div>
  );
}
